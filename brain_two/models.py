from django.db import models


class Todolist(models.model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)
