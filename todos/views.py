from django.shortcuts import render
from .models import TodoList

# Create your views here.


def todo_list_list(request):
    table = TodoList.objects.all()
    context = {
        "todo_list_list": table,
    }
    return render(request, "todo_list/list.html", context)
